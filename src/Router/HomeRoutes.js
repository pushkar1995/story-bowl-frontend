import React from 'react'
import { BrowserRouter, Switch, Route } from 'react-router-dom'
import { Container } from '@material-ui/core';

import TopBar from '../Components/TopBar/TopBar';
import ProjectBoards from '../Pages/ProjectBoards/ProjectBoards'
import Coverage from '../Pages/Coverage/Coverage';
import Plot from '../Pages/Plot/Plot';
// import Auth from './components/Auth/Auth'

function HomeRoutes() {
    return (
    <BrowserRouter>
        <Container>
            <TopBar />
            <Switch>
                <Route path='/' exact component={ProjectBoards} />
                <Route path='/coverage' exact component={Coverage} />
                <Route path='/plot' exact component={Plot} />
                {/* <Route path='/auth' exact component={Auth} /> */}
            </Switch>
        </Container>
    </BrowserRouter>
    )
}

export default HomeRoutes


