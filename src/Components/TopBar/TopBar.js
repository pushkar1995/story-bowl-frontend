import React from 'react'
import './TopBarStyles.scss'
import { Link } from 'react-router-dom'

function TopBar() {
    return (
    <nav className="navbar navbar-expand-lg navbar-light ">
        <div className="container-fluid">
            <Link className="navbar-brand" to="#">Navbar w/ text</Link>
            <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"></span>
            </button>
            <div className="collapse navbar-collapse" id="navbarText">
            <ul className="navbar-nav me-auto mb-2 mb-lg-0">
                <li className="nav-item">
                <Link className="nav-link active" aria-current="page" to="/">Project Boards</Link>
                </li>
                <li className="nav-item">
                <Link className="nav-link" to="/plot">Plot</Link>
                </li>
                <li className="nav-item">
                <Link className="nav-link" to="/coverage">Coverage</Link>
                </li>
            </ul>
            <span className="navbar-text">
                User Credentials
            </span>
            </div>
        </div>
    </nav>
    )
}

export default TopBar
