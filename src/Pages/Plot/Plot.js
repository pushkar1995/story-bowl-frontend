import React from 'react'
import { Link } from 'react-router-dom'
import { CircularProgressbar } from 'react-circular-progressbar';
import './PlotStyles.scss'

function Plot() {
    return (
        <div className="content mx-auto mt-5 ">
            <div className="d-flex header justify-content-center align-items-center">
                <span className="mr-auto d-flex">
                    {/* <div className="circle-progress mr-3 text-center pt-2 position-relative "></div> */}
                    <div  className=" position-relative mr-2" style={{ width: 50, height: 50, fontSize: "20px;" }}> 
                        <h5 className="font-weight-bold position-absolute text-circle">2/2</h5>
                        <CircularProgressbar 
                            styles={{
                                text: {
                                fill: '#0000ff',
                                fontSize: '20px',
                                },
                            }}                        
                        // value={this.state.value} 
                        maxValue={1}
                        />
                    </div>
                        <h3 className="font-weight-bold">Plot</h3>
                </span>
                <div>
                    <button className="text-white btn share-top-right font-weight-bold " style={{"minWidth":"200px"}}>
                        รับชมวิดีโอสอน
                    </button>
                </div>
            </div>
    
            <div class="card bg-card-board mb-4 border-0 ">
              <div class="card-body pl-5  ">
                <div className="d-flex align-items-center">
                    <span className="font-weight-bold font-18 color-header-board">
                            <span className="a-click mr-5 ">
                                Lorem ipsum, dolor sit amet consectetur adipisicing elit. Dicta sapiente, corporis impedit 
                            </span>
                    </span>
                </div>
              </div>
            </div>


            <div className="card bg-card-board mb-5  border-0 ">
              <div className="card-body ">
                  
                  <div className="mb-5 position-relative">
                    <div className="act font-weight-bold text-black  p-1 pl-3">
                        <li>Act 1</li>
                    </div>
            
                      <div class="card-body pt-2">
                          <div className="d-flex font-15 ">
                                <span className="font-15 font-weight-bold mr-auto color-gray">กรุณาระบุเนื้อหา...</span>
                          </div>
                      </div>
                    </div>
                    
                    <hr className="divider"></hr>

                  <div className="mb-5 position-relative ">
                   
                    <div className="act font-weight-bold text-black p-1 pl-3">
                        <li>Act 2</li>
                    </div>
               
                      <div class="card-body pt-2">
                          <div className="d-flex  font-15">
                            <span className="font-15 font-weight-bold mr-auto color-gray">กรุณาระบุเนื้อหา...</span>
                          </div>
                      </div>
                    </div>
                 
                    <hr className="divider"></hr>

                  <div className="mb-5 position-relative">
                    <div className="act font-weight-bold text-black p-1 pl-3">
                        <li>Act 3</li>
                    </div>
             
                      <div class="card-body pt-2">
                          <div className="d-flex ">
                            <span className="font-15 font-weight-bold mr-auto color-gray">กรุณาระบุเนื้อหา...</span>
                          </div>
                      </div>
                    </div>
              </div>
            </div>

            <div className="d-flex">
                <div className="d-inline">
                    <button className="btn bg-blue mb-3 text-white font-weight-bold  mr-2">
                        บันทึก
                    </button>
                    <button className="btn share-top-right mb-3 text-white font-weight-bold  mr-2 ">
                        ลบข้อมูล
                    </button>
                </div>
                <div className="d-inline">
                    <Link to="/coverage">
                        <button className="btn share-button text-white font-weight-bold mb-3  mr-2 ">
                            ย้อนกลับ
                        </button>
                    </Link>
                    <Link to="/plot_success">
                        <button className="btn bg-blue text-white font-weight-bold mb-3  mr-2 ">
                            เสร็จสิ้น
                        </button>                      
                    </Link>
                </div>
            </div>
        </div>
    )
}

export default Plot
