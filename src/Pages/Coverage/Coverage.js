import React from 'react'
import { Link } from 'react-router-dom'
import { CircularProgressbar } from 'react-circular-progressbar';
import './CoverageStyles.scss'

function Coverage() {
    return (
        <div className="content mx-auto mt-5 coverage-container">
        <div className="d-flex header justify-content-center align-items-center">
            <span className="mr-auto d-flex">
                {/* <div className="circle-progress mr-3 text-center pt-2 position-relative "></div> */}
                <div class="row d-flex justify-content-center align-items-center ">
                <div  className=" position-relative mr-2" style={{ width: 50, height: 50, fontSize: "20px;" }}> 
                    <h5 className="font-weight-bold position-absolute text-circle">1/2</h5>
                    <CircularProgressbar 
                    styles={{
                        text: {
                          fill: '#4b9cdc',
                          fontSize: '20px',
                        },
                    }}                        
                    // value={this.state.value} 
                    maxValue={1} 
                    />
                </div>
                    <h3 className="font-weight-bold">Coverage</h3>
                </div>
               
            </span>
            <div className="header-right-buttons">
                <button className="text-white btn share-top-right" style={{"minWidth":"200px"}}>รับชมวิดีโอสอน</button>
            </div>
        </div>
 
        <div class="card mb-2 border-0 ">
          <div class="card-body bg-card-board pl-2  ">
                    <div className=" align-items-center" style={{flexWrap: 'wrap'}}>
                        <span className="font-weight-bold font-18">
                            คำแนะนำการกรอกข้อมูล Coverage : &nbsp;&nbsp; 
                        </span>
                    </div>
          </div>
        </div>

        <div className=" ">
          <div className="card-body">
          <div className="row">
                <div  className=" a-click col-md-4">
                    <div class="card card-project  shasow_card">
                        <div class="card-body ">
                            <div className="d-flex">
                                <span className="font-25 color-header-board font-weight-bold mr-auto">TITLE</span>
                            </div>
         
                                <div className="mt-2 ml-2  font-25 font-weight-bold op-harf">
                                    กรุณาระบุเนื้อหา...
                                </div>

                                <div className="mt-2 ml-2 font-25 font-weight-bold op-harf">
                                   BY กรุณาระบุเนื้อหา...
                                </div>
                        </div>
                    </div>        
                </div>
                <div  className=" a-click col-md-8">
                    <div class="card card-project  shasow_card">
                        <div class="card-body">
                        <div className="d-flex">
                            <span className="font-25 color-header-board font-weight-bold mr-auto">PREMISE*</span>
                            </div>
                                <div className="mt-2 ml-2 font-18 font-weight-bold op-harf">
                                    กรุณาระบุเนื้อหา...
                                </div>

                                <div className="mt-2 ml-2  font-25 font-weight-bold op-harf">
                                   ..
                                </div>
                          
                        </div>
                    </div>
                </div>
            </div>

            {/* genre */}
            <div className="row mt-5 ">
                <div  className="col-md-4  a-click genreContainer">
                    <div class="card card-project  shasow_card">
                        <div class="card-body ">
                            <div className="d-flex">
                                <span className="font-25 color-header-board font-weight-bold mr-auto">GENRE*</span>
                            </div>
                                <div className="mt-2 ml-2 font-18 font-weight-bold op-harf">
                                    Style:
                                </div>
                                <div className="mt-2 ml-2 font-18 font-weight-bold op-harf">
                                    Mood:
                                </div>
                        </div>
                    </div>        
                </div>
                
                <div className="col-md-8">
                        <div class="card card-project  shasow_card">
                            <div class="card-body">
                            <div className="d-flex">
                                <span className="font-25 color-header-board font-weight-bold mr-auto">THEME*</span>
                                </div>
                                    <div className="mt-2 ml-2 font-18 font-weight-bold op-harf">
                                        กรุณาระบุเนื้อหา...
                                    </div>
                            </div>
                        </div>
                    </div>
            </div>

       
            <div className="row mt-4 ">
                <div className="col-md-12 a-click ">
                    <div class="card card-project-long  shasow_card">
                        <div class="card-body ">
                            <div className="d-flex">
                                <span className="font-218 color-header-board font-weight-bold mr-auto">LOCK STORY LINE*</span>
                            </div>
                        
                            <div className="mt-2 ml-2 font-18 font-weight-bold op-harf">
                                กรุณาระบุเนื้อหา...
                            </div>
                            
                        </div>
                    </div>        
                </div>
            </div>



            <div className="row mt-4 ">

                <div className="col-md-8 a-click " >
                    <div class="card card-project  shasow_card">
                        <div class="card-body">
                        <div className="d-flex">
                            <span className="font-25 color-header-board font-weight-bold mr-auto">LOCK LINE*</span>
                            </div>
                                    <div className="mt-2 ml-2 font-18 font-weight-bold op-harf">
                                        กรุณาระบุเนื้อหา...
                                    </div>
                        </div>
                    </div>
                </div>

                <div className="col-md-4 a-click">
                    <div className="card card-project  shasow_card">
                        <div className="card-body ">
                            <div className="d-flex">
                                <span className="font-25 color-header-board font-weight-bold mr-auto">CHECK BOX*</span>
                            </div>
                        
                                <div className="mt-2 ml-2 font-25 font-weight-bold op-harf">
                                    กรุณาระบุเนื้อหา...
                                </div>
            

                        </div>
                    </div>        
                </div>

            </div>
          </div>

        </div>
        <div className="d-flex justify-content-between mb-4">
            <div className="d-inline">
                <button className="btn bg-blue mb-3 text-white font-weight-bold  mr-2 " style={{width:"100px"}}>
                    บันทึก
                </button>
                <button className="btn share-top-right mb-3 text-white font-weight-bold  mr-2 " style={{width:"100px"}}>
                    ลบข้อมูล
                </button>
            </div>
                <div className="d-inline">
                    <Link to="/board">
                        <button className="btn share-top mw-180 text-white font-weight-bold  mr-2 mb-3" style={{width:"100px"}}>
                            ย้อนกลับ
                        </button>
                    </Link>
                    <Link to="/plot">
                        <button className="btn  bg-blue mw-180 text-white font-weight-bold  mr-2 mb-3" style={{width:"100px"}}>
                            ถัดไป
                        </button>
                    </Link>
                </div>
        </div>
        </div>
    )
}

export default Coverage
