import React from 'react'
import { Link } from 'react-router-dom'
import './ProjectBoardsStyles.scss';

function ProjectBoards() {
    return (
        <div className="content mx-auto mt-5">
            <div className="d-flex header justify-content-center align-items-center">
                <span className="mr-auto d-flex bg-card-board">
                    <h3 className="font-weight-bold">PROJECT BOARDS</h3>
                </span>
                <div className="d-flex header justify-content-space-around align-items-center">
                    <div>
                        <p>DOING</p>
                    </div>
                    <div>
                        <p>DONE</p>
                    </div>
                </div>

                <div>
                    {/* <button style={{"minWidth":"150px"}} className="text-white btn share-top mr-2">share</button> */}
                    <Link style={{"minWidth":"50px"}} className="text-white share-top-right">Edit</Link>
                </div>
            </div>

       
            <div className="card  mb-5 border-0 ">
              <div className="card-body ">                
                <div className="row mt-4">
                    <div className="col-md-4 mt-4">
                          <div class="card card-project bg-card-board border-0">
                              <div class="card-body a-click">
                                  <p className=" text-center mt-2 color-gray">
                                      <i class="fas fa-plus font-40"></i>
                                  </p>
                                  <div className="font-weight-bold font-20  color-gray text-center">
                                      สร้าง Project ใหม่
                                  </div>
                              </div>
                          </div>
                    </div>


                    <div className="col-md-4 mt-4">
                          <div class="card card-project bg-card-board border-0">
                              <div class="card-body a-click">
                                  <p className=" text-center mt-2 color-gray">
                                      This is Done Projects
                                  </p>
                                  <div className="font-weight-bold font-20  color-gray text-center">
                                      สร้าง Project ใหม่
                                  </div>
                              </div>
                          </div>
                    </div>

                </div>
              </div>
            </div>
           
        </div>
    )
}

export default ProjectBoards
